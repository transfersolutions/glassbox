using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCReceiver : MonoBehaviour
{
    public OSC osc;
    private List<PersonBlob> blobs = new List<PersonBlob>();
    public GameObject blobPrefab, background;

    private int lastId = -1;
    private int nrCols = 0;

    // Start is called before the first frame update
    void Start()
    {
        osc.SetAddressHandler("/point", OnReceivePoint);
    }

    void OnReceivePoint(OscMessage message)
    {
        int id = message.GetInt(0);
        int userId = message.GetInt(1);
        float x = message.GetFloat(2);
        float y = message.GetFloat(3);
        //float width = message.GetFloat(4);
        //float height = message.GetFloat(5);

        Debug.Log("Searching for blob " + userId);

        /* search for Blob with Id userId (if none found, init) and call updateMovement. */
        PersonBlob blob = blobs.Find(blob => blob.id == userId);
        if(blob == null) {
            GameObject blobGO = Instantiate(blobPrefab);
            blobGO.transform.parent = gameObject.transform;
            blob = blobGO.GetComponent<PersonBlob>();
            blob.id = userId;
            blob.background = background;
            blobs.Add(blob);
            Debug.Log("New blob " + userId);
        }
        blob.addPoint(x, y);

        if(lastId != -1 && lastId != userId) {
            blob.createCollider();
            nrCols++;
            Debug.Log("collider " + nrCols);
        }
        lastId = userId;
    }
}
