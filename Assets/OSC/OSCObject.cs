using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSCObject : MonoBehaviour
{
    public OSC osc;
    public GameObject background;
    public float smoothTime = 0.3f;
    private Vector2 oscSize = new Vector2(3.36f, 3);
    public float collisionDistance = 0.02f;
    private float speed = 0;

    private Vector3 size;
    private Vector3 direction, targetScale;

    // Start is called before the first frame update
    void Start()
    {
        osc.SetAddressHandler("/point", OnReceivePlayer);
        SpriteRenderer renderer = background.GetComponent<SpriteRenderer>();
        size = renderer.bounds.size;
        direction = transform.position;
        targetScale = transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 lerpy = Vector3.Lerp(transform.position, direction, smoothTime);
        speed = Vector3.Distance(lerpy, transform.position);
        transform.position = lerpy;
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, smoothTime);
    }
    
    private void OnCollisionEnter2D(Collision2D other) 
    {
        GameObject otherObject = other.gameObject;
        if(otherObject.CompareTag("Vase")) {
            // Debug.Log(speed + " > " + collisionDistance + "?");
            if(speed > collisionDistance) {
                otherObject.GetComponent<VaseBreaker>().breakVase();
            }
        }
    }

    void OnReceivePlayer(OscMessage message)
    {
        int id = message.GetInt(0);
        int userId = message.GetInt(1);
        float x = message.GetFloat(2);
        float y = message.GetFloat(3);
        //float width = message.GetFloat(4);
        //float height = message.GetFloat(5);
        float width = 1;
        float height = 1;

        Debug.Log("Point " + id + " for " + userId + " (" + x + "," + y + ")");

        direction = new Vector3(x, y, 0);
        targetScale = new Vector3(width, height, 1);
    }
}
