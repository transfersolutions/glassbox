using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonBlob : MonoBehaviour
{
    public GameObject background;
    // public float smoothTime = 0.3f;
    public float collisionDistance = 0.02f;
    // private float speed = 0;
    public float id;

    // private Vector3 size;
    // private Vector3 direction, targetScale;

    private List<Vector2> points = new List<Vector2>();
    private List<Vector3> positions = new List<Vector3>();

    private PolygonCollider2D polyCollider;
    private LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        // direction = transform.position;
        // targetScale = transform.localScale;
        polyCollider = GetComponent<PolygonCollider2D>();
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /*Vector3 lerpy = Vector3.Lerp(transform.position, direction, smoothTime);
        speed = Vector3.Distance(lerpy, transform.position);
        transform.position = lerpy;
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, smoothTime);*/
    }
    
    private void OnCollisionEnter2D(Collision2D other) 
    {
        GameObject otherObject = other.gameObject;
        if(otherObject.CompareTag("Vase")) {
            Debug.Log("Relative velocity: " + other.relativeVelocity.magnitude);
            if(other.relativeVelocity.magnitude > collisionDistance) {
                otherObject.GetComponent<VaseBreaker>().breakVase();
            }
        }
    }

    public void addPoint(float x, float y) {
        points.Add(new Vector2(x, y));
        positions.Add(new Vector3(x, y, 0));
    }

    public void createCollider() {
        polyCollider.points = points.ToArray();
        lineRenderer.positionCount = positions.Count;
        lineRenderer.SetPositions(positions.ToArray());
        points.Clear();
        positions.Clear();
    }
}
