using UnityEngine;

public class Drag : MonoBehaviour 
{
    private Vector3 _dragOffset;
    public Camera cam;
    private Rigidbody2D rb;

    [SerializeField] public float speed = 10;

    void Awake() 
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnMouseDown() 
    {
        _dragOffset = transform.position - GetMousePos();
    }

    void OnMouseDrag() 
    {
        Vector3 offset = GetMousePos() + _dragOffset - transform.position;
        rb.velocity = new Vector2(offset.x, offset.y) * speed; 
        //Vector3.MoveTowards(rb.velocity, GetMousePos() + _dragOffset, speed * Time.deltaTime) ;
    }

    Vector3 GetMousePos() 
    {
        var mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        return mousePos;
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        GameObject otherObject = other.gameObject;
        if(otherObject.CompareTag("Vase")) {
            //Debug.Log(rb.velocity.magnitude + " > " + 2 * speed + "?");
            if(rb.velocity.magnitude > 2 * speed) {
                otherObject.GetComponent<VaseBreaker>().breakVase();
            }
        }
    }
}