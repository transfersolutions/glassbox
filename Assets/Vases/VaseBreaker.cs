using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseBreaker : MonoBehaviour
{
    public GameObject shards;

    public void breakVase() {
        GameObject shardObj = Instantiate(shards, transform.position, shards.transform.rotation);
        VaseRegrower regrower = shardObj.GetComponent<VaseRegrower>();
        regrower.vase = this.gameObject;
        this.gameObject.SetActive(false);
    }
}
