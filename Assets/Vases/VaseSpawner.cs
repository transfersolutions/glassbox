using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseSpawner : MonoBehaviour
{
    public GameObject vase;
    public Sprite[] sprites;
    public int nrVases;

    public PhysicsMaterial2D material;

    // Start is called before the first frame update
    void Start()
    {   
        InvokeRepeating("SpawnVase", 1, .5f);
    }

    private int i = 0;
    void SpawnVase()
    {
        if(i < nrVases) {
            GameObject newObject = Instantiate(vase);//, new Vector3(x, y, 0), new Quaternion(0, 0, 0, 0));
            newObject.transform.parent = gameObject.transform;
            if(sprites.Length > 0) {
                SpriteRenderer renderer = newObject.AddComponent<SpriteRenderer>();
                renderer.sprite = sprites[i % sprites.Length];
                newObject.transform.localScale = new Vector3(0.2f, 0.2f, 1);
                PolygonCollider2D collider = newObject.AddComponent<PolygonCollider2D>();
                collider.sharedMaterial = material;
            }
        } else {
            CancelInvoke("SpawnVase");
        }

        i++;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
