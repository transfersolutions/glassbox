using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VaseRegrower : MonoBehaviour
{
    public GameObject vase;

    private void OnParticleCollision(GameObject otherObject) 
    {
        Debug.Log("Collision!");
        if(otherObject.CompareTag("Floor")) {
            Debug.Log("Floor!");
            vase.transform.position = this.transform.position;
            vase.SetActive(true);
            Destroy(this.gameObject);
        }
    }

    private void OnParticleSystemStopped() {
        Vector3 vaseSize = vase.GetComponent<SpriteRenderer>().bounds.size;
        // Debug.Log("Stopped at " + this.transform.position + ". Vase: " + vaseSize);
        vase.transform.position = this.transform.position;
        vase.SetActive(true);
        Destroy(this.gameObject);
    }
}
